﻿using OpenQA.Selenium;
using OpenQA.Selenium.Remote;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace C1Wijmo5.TestScripts.Extend
{
    public class BaseTestElement
    {
         protected RemoteWebDriver browser;
        public BaseTestElement(RemoteWebDriver Browser)
        {
            browser = Browser;
        }
        public virtual string FindElementCssStyle { get; set; }

        public IWebElement Element
        {
            get { return browser.FindElementByCssSelector(FindElementCssStyle); }
        }
    }
}
