﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace C1Wijmo5.TestScripts.Extend
{
    public class Utility
    {
        // there have several pattern for the  MyanmarTime
        // 1. +0630(Myanmar Time)
        // 2. +0630(MyanmarStandardTime)
        // 3. +0630 (Myanmar Time)
        // 4. +0630 (缅甸时间)
        public static string RemoveMyanmarTime(string message)
        {
            Regex reg = new Regex(@"\+0630\s?\([^\)]*\)");
            return reg.Replace(message, @"+0630");

        }
        // firefox return rgb
        // chrome and ie return rgba
        // so convert result from rgba to rgb
        public static string ConvertRGBA_to_RGB(string message)
        {
            Regex reg = new Regex(@"rgba\((\s?\d+,\s?\d+,\s?\d+),[^\)]*\)");
            return reg.Replace(message, @"rgb($1)").Replace(" ","");

        }
    }
}
