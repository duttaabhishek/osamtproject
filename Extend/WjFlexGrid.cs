﻿using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Remote;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace C1Wijmo5.TestScripts.Extend
{
    public class WjFlexGrid : BaseTestElement
    {
        public WjFlexGrid(RemoteWebDriver Browser): this(Browser,".wj-flexgrid") { }

        public WjFlexGrid(RemoteWebDriver Browser,string FindGridCssStyle) : base(Browser) {
            FindElementCssStyle = FindGridCssStyle;
        }

        public IWebElement GetCell(int row, int col)
        {

            return this.Element.FindElement(By.ClassName("wj-cells")).FindElements(By.ClassName("wj-row"))[row].FindElements(By.ClassName("wj-cell"))[col];
        }

        public IWebElement GetRowHeaderCell(int index)
        {

            return this.Element.FindElement(By.ClassName("wj-rowheaders")).FindElements(By.ClassName("wj-header"))[index];
        }
        public IWebElement GetColHeaderCell(int index)
        {

            return this.Element.FindElement(By.ClassName("wj-colheaders")).FindElements(By.ClassName("wj-header"))[index];
        }
        public void ClickTopLeft()
        {
            GetTopLeft().Click();
        }

        public void ClickBottomLeft()
        {
            this.Element.FindElement(By.ClassName("wj-bottomleft")).FindElement(By.ClassName("wj-header")).Click();
        }
        private IWebElement GetTopLeft()
        {
            return this.Element.FindElement(By.ClassName("wj-topleft")).FindElement(By.ClassName("wj-header"));
        }

        public void ClickRowheader()
        {
            this.Element.FindElement(By.ClassName("wj-rowheaders")).FindElement(By.ClassName("wj-cell")).Click();
        }

        public void CellContextClick(int row, int col)
        {

            new Actions(browser).ContextClick(GetCell(row, col)).Perform();

            
        }
        public void TopLeftContextClick()
        {

            new Actions(browser).ContextClick(GetTopLeft()).Perform();


        }
        public void DragAndDropElement(IWebElement SourceElement, IWebElement TargetElement)
        {
            new Actions(browser).ClickAndHold(SourceElement)
                                   .MoveToElement(TargetElement).Release().Perform();
        }

        public void DragAndDropCell(int SourceCellRowIndex, int SourceCellColIndex, int TargetCellRowIndex, int TargetCellColIndex)
        {
            this.DragAndDropElement(this.GetCell(SourceCellRowIndex, SourceCellColIndex), this.GetCell(TargetCellRowIndex, TargetCellColIndex));
        }

        public void DragAndDropRowHeaderCell(int SourceCellIndex, int TargetCellIndex)
        {
            this.DragAndDropElement(this.GetRowHeaderCell(SourceCellIndex), this.GetRowHeaderCell(TargetCellIndex));
        }
        public void DragAndDropColHeaderCell(int SourceCellIndex, int TargetCellIndex)
        {
            this.DragAndDropElement(this.GetColHeaderCell(SourceCellIndex), this.GetColHeaderCell(TargetCellIndex));
        }
    }

    public class WjColumnfiltereditor : BaseTestElement
    {

        public WjColumnfiltereditor(RemoteWebDriver Browser) : base(Browser) { }

        public override string FindElementCssStyle => ".wj-columnfiltereditor";
        public void SetSearchBoxText(string text)
        {
            Element.FindElement(By.CssSelector(".wj-input input")).SendKeys(text);
        }

        public void SetDivVal1(string text)
        {
            Element.FindElement(By.CssSelector("[wj-part='div-val1'] input")).SendKeys(text); 
        }
        public void ClickSelectAll()
        {
            Element.FindElement(By.CssSelector("[wj-part='cb-select-all']")).Click();
        }
        public void ClickApply()
        {
            Element.FindElement(By.CssSelector("[wj-part='btn-apply']")).Click();
        }

        public void ClickListItem(int index)
        {
            var parent = Element.FindElement(By.CssSelector(".wj-valuefilter-editor .wj-listbox"));
            var item = parent.FindElements(By.CssSelector("input"))[index];
            if (!item.Displayed)
            {
                for (int i = 0; i < index; i++)
                {
                    parent.SendKeys(Keys.ArrowDown);
                }
            }
          
            item.Click();
           
        }

     

    }
}
