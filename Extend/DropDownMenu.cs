﻿using OpenQA.Selenium;
using OpenQA.Selenium.Remote;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace C1Wijmo5.TestScripts.Extend
{

    public class BaseMenu
    {
        private RemoteWebDriver browser;
        private string menuname = ".wj-dropdown-panel";
        private int menuindex = 0;
        
        protected virtual string menuFindCssStyle { get; }
        public BaseMenu(RemoteWebDriver Browser):this(Browser,0)
        {
          
        }
        public BaseMenu(RemoteWebDriver Browser, int menuIndex)
        {
            browser = Browser;
            menuindex = menuIndex;
        }

        private IWebElement Element
        {
            get { return browser.FindElementsByCssSelector(menuname)[menuindex]; }
        }

        public void SendKeys(string text, int count = 1)
        {
            for (int i = 0; i < count; i++)
            {
                this.Element.SendKeys(text);
            }
        }
    }
    public class DropDownMenu:BaseMenu
    {
        public DropDownMenu(RemoteWebDriver Browser) : this(Browser, 0)
        {
        }

        public DropDownMenu(RemoteWebDriver Browser, int menuIndex):base(Browser,menuIndex)
        {
         
        }
        protected override string menuFindCssStyle => ".wj-dropdown-panel";
    }

    public class DropDownList:BaseMenu
    {
        private RemoteWebDriver browser;
       
        protected override string menuFindCssStyle => ".wj-dropdown-panel .wj-content";
        public DropDownList(RemoteWebDriver Broswers):base(Broswers)
        {
          
        }

     
    }
}
