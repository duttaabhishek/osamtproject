﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Remote;

namespace C1Wijmo5.TestScripts.Extend
{
    public class TestBrowser
    {
        private RemoteWebDriver driver;
        private Actions actions;
        public TestBrowser(RemoteWebDriver Driver )
        {
            driver = Driver;
            actions = new Actions(Driver);
        }

        public void SendKeys(string text)
        {
            new Actions(driver).SendKeys(text).Perform();
        }

        public void SendKeys(IWebElement element, string text)
        {
            SendKeys(element,text,1);
        }

        public void SendKeys(IWebElement element, string text,int times)
        {
            for(int i=0;i<times; i++)
            {
                element.SendKeys(text);
            }
           
        }
        public void KeyUp(string text)
        {
           // actions.KeyUp(text).Perform();
            new Actions(driver).KeyUp(text).Perform();
        }
        public void KeyDown(string text)
        {
            //actions.KeyDown(text);
            new Actions(driver).KeyDown(text).Perform();
        }

        public void Clear(IWebElement Element)
        {
            Element.SendKeys(Keys.Control + "a");
            Element.SendKeys(Keys.Delete);
        }

        public void ContextClick(IWebElement element)
        {
            
            new Actions(driver).ContextClick(element).Perform();
        }
        public  void DragAndDropElements( IWebElement SourceElement, IWebElement TargetElement)
        {

            new Actions(driver).ClickAndHold(SourceElement)
                                        .MoveToElement(TargetElement).Release().Perform();

        }
    }
}
