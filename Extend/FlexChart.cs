﻿using OpenQA.Selenium;
using OpenQA.Selenium.Remote;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Drawing;
using System.Linq;
using System.Text;

namespace C1Wijmo5.TestScripts.Extend
{

    public class WjFlexChart : WebElement
    {

        public WjFlexChart(IWebElement element, ISearchContext parent) : base(element, parent)
        {
        }

        public static WjFlexChart CreateChart(ISearchContext parent)
        {
            return new WjFlexChart(null, parent);
        }
        protected override string FindCssSytle => ".wj-flexchart";


        public SeriesGroup WjSeriesGroup => new SeriesGroup(null, this);
        public g WjDataLabels => new g(null, this, ".wj-data-labels");
        public Axis Axisx => new Axis(null, this, ".wj-axis-x");
        public WebElements<Axis> Axisxs => new WebElements<Axis>(this, ".wj-axis-x");
        public Axis Axisy => new Axis(null, this, ".wj-axis-y");
        public g Axisylabels => new g(null, this, ".wj-axis-y-labels");
        public WebElements<Datalabel> datalabels => new WebElements<Datalabel>(this, ".wj-data-label");
        public SliceGroup WjSliceGroup => new SliceGroup(null, this);

        public g header => new g(null, this, ".wj-header");

        public g footer => new g(null, this, ".wj-footer");

        public Legend legend => new Legend(null, this);


        public class SeriesGroup : g
        {
            public SeriesGroup(IWebElement element, ISearchContext parent) : base(element, parent) { }

            protected override string FindCssSytle => ".wj-series-group";

            public WebElements<Path> paths => new WebElements<Path>(this, "path");

            public g WaterFallConnectorLines => new g(null, this, ".water-fall-connector-lines");


        }



        public class Axis : g
        {
            public Axis(IWebElement element, ISearchContext parent) : this(element, parent, "") { }
            public Axis(IWebElement element, ISearchContext parent, string cssStyle) : base(element, parent, cssStyle) { }

            public WebElements<Line> gridlines => new WebElements<Line>(this, ".wj-gridline");

            public WebElements<GridlineMinor> gridlineminors => new WebElements<GridlineMinor>(this, ".wj-gridline-minor");

            public WebElements<Line> ticks => new WebElements<Line>(this, ".wj-tick");

            public WebElements<Datalabel> datalabels => new WebElements<Datalabel>(this, ".wj-data-label");



            public class GridlineMinor : Line
            {
                public GridlineMinor(IWebElement element, ISearchContext parent) : base(element, parent) { }
                protected override string FindCssSytle => ".wj-gridline-minor";
                public string points => this.GetAttributeStringReplaceSpaceToComma("points");

            }

        }

        public class Legend :g
        {
            public Legend(IWebElement element, ISearchContext parent) : this(element, parent, ".wj-legend") { }
            public Legend(IWebElement element, ISearchContext parent, string cssStyle) : base(element, parent, cssStyle) { }

            public void ClickByName(string name)
            {
                ClickItem(this.labels.Where(t => t.Text == name).ToList()[0]);
            }
            public virtual void ClickByIndex(int index)
            {
                ClickItem(this.labels[index]);

            }
            protected  void ClickItem(IWebElement item)
            {
                item.Click();
            }

        }
        public class PolyLine : WebElement
        {
            public PolyLine(IWebElement element, ISearchContext parent) : base(element, parent) { }

            protected override string FindCssSytle => "polyline";
            public string points => GetAttributeStringReplaceSpaceToComma("points");

            public string stroke => this.GetAttributePlusSeparation("stroke");

            public string strokewidth => this.GetAttributePlusSeparation("stroke-width");


        }

        public class PolyLines : WebElements<PolyLine>
        {
            public PolyLines(IWebElement element) : base(element) { }
            protected override string FindcssStyle => "polyline";

            public string GetPointsString()
            {
                return ToString((x) => { return x.points; });
            }


        }


        public class Rect : WebElement
        {
            public Rect(IWebElement element, ISearchContext parent) : base(element, parent) { }
            protected override string FindCssSytle => "rect";
            public string stroke => this.GetAttributePlusSeparation("stroke");
           
            public string height => this.GetAttributePlusSeparation("height");
            public string width => this.GetAttributePlusSeparation("width");
        }

        public class Polygon : WebElement
        {
            public Polygon(IWebElement element, ISearchContext parent) : base(element, parent) { }
            protected override string FindCssSytle => "polygon";
            public string points => GetAttributeStringReplaceSpaceToComma("points");

        }

    

        public class g : WebElement
        {
            public g(IWebElement element, ISearchContext parent) : this(element, parent,"g") { }
            public g(IWebElement element, ISearchContext parent,string cssStyle) : base(element, parent, cssStyle) { }
          
            public PolyLines polylines => new PolyLines(this);
            public Text title => new Text(null, this, ".wj-title");
            public WebElements<Rect> rects => new WebElements<Rect>(this, "rect");
            public WebElements<Ellipse> ellipses => new WebElements<Ellipse>(this, "ellipse");
            public WebElements<Polygon> polygons => new WebElements<Polygon>(this, "polygon");
            public Polygon polygon => polygons[0];
            public WebElements<g> gs => new WebElements<g>(this,"g");
            public WebElements<Text> TextElements => new WebElements<Text>(this, "text");
            public WebElements<Text> labels => new WebElements<Text>(this, ".wj-label");
            public WebElements<Line> lines => new WebElements<Line>(this,"line");
        }
    

        public class Datalabel : WebElement
        {
            public Datalabel(IWebElement element, ISearchContext parent) : base(element, parent) { }
            protected override string FindCssSytle => ".wj-data-label";
            public string classname => this.GetAttributePlusSeparation("class");

        }
      
        public class Line : WebElement
        {
            public Line(IWebElement element, ISearchContext parent) : base(element, parent) { }
            protected override string FindCssSytle => "line";
            public string x1 => this.GetAttributePlusSeparation("x1");
            public string y1 => this.GetAttributePlusSeparation("y1");
            public string x2 => this.GetAttributePlusSeparation("x2");
            public string y2 => this.GetAttributePlusSeparation("y2");
            public string stroke => this.GetAttributePlusSeparation("stroke");
            public override string ToString()
            {
                return "x1:" + x1 + "x2:" + x2 + "y1:" + y1 + "y2:" + y2 ;
            }
        }
      
        public class Text : WebElement
        {
            public Text(IWebElement element, ISearchContext parent) : this(element, parent,"text") { }
            public Text(IWebElement element, ISearchContext parent ,string cssStyle) : base(element, parent, cssStyle) { }
        
        }
       
        public class Path : WebElement
        {
            public Path(IWebElement element, ISearchContext parent) : this(element, parent,"path") { }
            public Path(IWebElement element, ISearchContext parent,string cssStyle) : base(element, parent,cssStyle) { }
         
            public string d => this.GetAttributeStringReplaceSpaceToComma("d");
            public string classname => this.GetAttributePlusSeparation("class");

        }
     
        public class Ellipse : WebElement
        {
            public Ellipse(IWebElement element, ISearchContext parent) : base(element, parent) { }
            protected override string FindCssSytle => "ellipse";
            public string stroke => this.GetAttributePlusSeparation("stroke");
            public string strokewidth => this.GetAttributePlusSeparation("stroke-width");
            public string cx => this.GetAttributePlusSeparation("cx");
            public string cy => this.GetAttributePlusSeparation("cy");
            public string rx => this.GetAttributePlusSeparation("rx");
            public string ry => this.GetAttributePlusSeparation("ry");

            public override string ToString()
            {
                return "cx:" +cx + "cy:"+cy +"rx:" + rx + "ry:" + ry ;
            }
        }
     

        public class SliceGroup : g
        {
            public SliceGroup(IWebElement element, ISearchContext parent) : base(element, parent) { }

            protected override string FindCssSytle => ".wj-slice-group";
            public WebElements<Path> paths => new WebElements<Path>(this, "path");

            public Path stateselected => new Path(null, this, ".wj-state-selected");

            public WebElements<Path> stateselecteds => new WebElements<Path>(this, ".wj-state-selected");

            public virtual void ClickByIndex(int index)
            {
                paths[index].Click();

            }
        }
    }

   
    
  

}
