﻿using OpenQA.Selenium;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace C1Wijmo5.TestScripts.Extend
{
    public class WebElements<T> : IEnumerable<T>
    {
        private ISearchContext _parent;
        protected virtual string FindcssStyle { get; set; }
        public WebElements(ISearchContext parent):this(parent,""){}

        public WebElements(ISearchContext parent,string cssStyle)
        {
            _parent = parent;
            FindcssStyle = cssStyle;
        }

        public T this[int index]
        {
            get
            {
                return (T)Activator.CreateInstance(typeof(T), _parent.FindElements(By.CssSelector(FindcssStyle))[index], _parent);
            }
        }

        public int Count => _parent.FindElements(By.CssSelector(FindcssStyle)).Count;

        public string ToString(Func<T, string> TypeProperty)
        {
            string result = "";
            this.ToList().ForEach(x => result += TypeProperty(x));
            return result;
        }

        IEnumerator<T> IEnumerable<T>.GetEnumerator()
        {
            for (int i = 0; i < Count; i++)
            {
                yield return this[i];
            }
        }

        public IEnumerator GetEnumerator()
        {
            for (int i = 0; i < Count; i++)
            {
                yield return this[i];
            }
        }
    }
}
