﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Drawing;
using System.Linq;
using System.Text;

namespace C1Wijmo5.TestScripts.Extend
{

    public class WebElement : IWebElement
    {
        private IWebElement webElement;
        private ISearchContext parent;
        public WebElement(IWebElement element) :this(element,null){}
        public WebElement(IWebElement element,ISearchContext Parent):this(element,Parent,""){}
        public WebElement(IWebElement element, ISearchContext Parent,string cssStyle)
        {
            this.webElement = element;
            this.parent = Parent;
            this.FindCssSytle = cssStyle;
        }

        protected  virtual string FindCssSytle { get; set; }
        public string Attributes_x_y => "x:" + x + "y:" + y;
        public string fill => this.GetAttributePlusSeparation("fill");
        public string x => this.GetAttributePlusSeparation("x");
        public string y => this.GetAttributePlusSeparation("y");
        public string TagName => Element.TagName;

        public string Text => Element.Text;

        public bool Enabled => Element.Enabled;

        public bool Selected => Element.Selected;

        public Point Location => Element.Location;

        public Size Size => Element.Size;

        public bool Displayed => Element.Displayed;

        public WebElement Self => this;

        public IWebElement Element {
            get
            {
                if (webElement == null && parent != null)
                {
                    return parent.FindElement(By.CssSelector(FindCssSytle));
                }
                return webElement;
                
            }
           }

        public void Clear()
        {
            Element.Clear();
        }

        public void Click()
        {
            Element.Click();
        }

        public IWebElement FindElement(By by)
        {
            return Element.FindElement(by);
        }

        public ReadOnlyCollection<IWebElement> FindElements(By by)
        {
            return Element.FindElements(by);
        }

        public string GetAttribute(string attributeName)
        {
            return Element.GetAttribute(attributeName);

        }

        public string GetAttributePlusSeparation(string attributeName)
        {
            return this.GetAttribute(attributeName) +",";

        }
        protected string GetAttributeStringReplaceSpaceToComma(string attributename)
        {
            // in chart control space Separation number , but in test assert we use commas
            return this.GetAttributePlusSeparation(attributename).Replace(" ", ",");
        }

        public string GetCssValue(string propertyName)
        {
            return Element.GetCssValue(propertyName);

        }

        public void SendKeys(string text)
        {
            Element.SendKeys(text);

        }

        public void Submit()
        {
            Element.Submit();

        }
    }
}
