﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Remote;
using System.IO;

namespace C1Wijmo5.TestScripts
{
    public  class C
    {
        public static string GroupHeaderRow = "wijmo-wijgrid-groupheaderrow";
        public static string DataRow = "wijmo-wijgrid-datarow";
        private static bool IE = false;
        public static bool Firefox = false;
        public static bool Chrome = false;

         static C()
        {
            IE= Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings["IE"]);
            Firefox= Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings["Firefox"]);
            Chrome = Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings["Chrome"]);
        }
     

        public static string Truncate(string msg)
        {
            msg = msg.Replace(" ", string.Empty);
            msg = msg.Replace("\"", string.Empty);
            msg = msg.Replace(Environment.NewLine, string.Empty);
            return msg;
        }

        public static bool IsFireFox(RemoteWebDriver browser)
        {
            return browser.GetType().Name == "FirefoxDriver";
        }
        public static bool IsIE(RemoteWebDriver browser)
        {
            return browser.GetType().Name == "InternetExplorerDriver";
        }
        public static bool IsChrome(RemoteWebDriver browser)
        {
            return browser.GetType().Name == "ChromeDriver";
        }

        public static string GetFlexGridData(RemoteWebDriver browser)
        {
            string data = browser.FindElementByClassName("wj-flexgrid").Text;
            return data;
        }

        
        private static RemoteWebDriver SetDriverImplicitWait( RemoteWebDriver driver )
        {
          
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);
            return driver;
        }
        private static FirefoxDriver CreadFirefoxDriver()
        {

            // return new FirefoxDriver();

            var ffProfile = new OpenQA.Selenium.Firefox.FirefoxProfile();
            ffProfile.DeleteAfterUse = true;
            int maxScriptRuntime = 120;//second.
            ffProfile.SetPreference("browser.chrome.toolbar_tips", false);
            ffProfile.SetPreference("dom.max_chrome_script_run_time", maxScriptRuntime);
            ffProfile.SetPreference("dom.max_script_run_time", maxScriptRuntime);

            ffProfile.SetPreference("browser.startup.page", 0);
            ffProfile.SetPreference("browser.startup.homepage", "about:blank");
            ffProfile.SetPreference("browser.startup.homepage_override.mstone", "ignore");
            ffProfile.SetPreference("browser.usedOnWindows10", true);
            ffProfile.SetPreference("browser.tabs.remote.force-enable", true);

            ffProfile.SetPreference("browser.tabs.crashReporting.sendReport", false);
            ffProfile.SetPreference("toolkit.asyncshutdown.crash_timeout", 100);



            var service = OpenQA.Selenium.Firefox.FirefoxDriverService.CreateDefaultService(AppDomain.CurrentDomain.BaseDirectory);
            var ffOptions = new OpenQA.Selenium.Firefox.FirefoxOptions();
            ffOptions.Profile = ffProfile;
            ffOptions.AddAdditionalCapability("acceptInsecureCerts", true, true);
            var fireWebdriver = new FirefoxDriver(service, ffOptions, new TimeSpan(0, 1, 0));

           
            return fireWebdriver;
        }

        private static void ExecuteTestMethod(string TestName, Type testClass)
        {
            object obj = Activator.CreateInstance(testClass);
            MethodInfo invoker = testClass.GetMethod(TestName);
            if (Chrome)
                invoker.Invoke(obj, new object[] { SetDriverImplicitWait(new ChromeDriver()) });
          
            if (Firefox)
                invoker.Invoke(obj, new object[] { SetDriverImplicitWait(CreadFirefoxDriver()) });

            if (IE)
                invoker.Invoke(obj, new object[] { SetDriverImplicitWait(new InternetExplorerDriver()) });

        }

        public static void Execute_Test(string TestName)
        {
            ExecuteTestMethod(TestName, typeof(Test_FlexGrid));
        }
       

    }
}
