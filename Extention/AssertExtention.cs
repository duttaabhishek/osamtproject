﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace C1Wijmo5.TestScripts.Extention
{
    public static partial class AssertEx
    {
        // compare the point pixel with delta
        public static void AreEqual( string expected, string actual,  string message, double delta=3.01)
        {

           if (!AreEqual(expected,actual,delta))
            {
                Assert.AreEqual(expected, actual, message);
            }
        }

        private static bool AreEqual(string expected, string actual,  double delta)
        {
    

            string[] sep = new string[] { ",", ":", "|","=",";", "(", ")",  ">" };
            var expectarray = NormalString(expected).Split(sep, StringSplitOptions.RemoveEmptyEntries);
            var actualarray = NormalString(actual).Split(sep, StringSplitOptions.RemoveEmptyEntries);

            if (expectarray.Length != actualarray.Length) return false;
            try
            {
                for (var i = 0; i < expectarray.Length; i++)
                {
                    double expectValue, actrualValue;
                    if (Double.TryParse(expectarray[i], out expectValue) && Double.TryParse(actualarray[i], out actrualValue))
                    {
                        Assert.AreEqual(expectValue, actrualValue, delta);
                    }
                    else
                    {
                        Assert.AreEqual(expectarray[i].Trim(), actualarray[i].Trim());
                    }
                }
            }
            catch
            {
                return false;
            }
            return true;
        }

        private static string NormalString(string instring)
        {
           return instring.Replace("px", "");
            
        }
    }
}
