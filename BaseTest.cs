﻿using GrapeCity.Testing.Tools.Kits;
using GrapeCity.Win.Testing.Tools;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;

namespace C1Wijmo5.TestScripts
{
    public abstract class BaseTest
    {
        public  string Get_URL(string page)
        {
            //return string.Format("http://localhost:5555/{0}/{1}",GetSubDirectory, page);
            var samplepath = new DirectoryInfo(AssemblyDirectory + "/../../../C1Wijmo5.TestSamples/" + GetSubDirectory);
            return string.Format("file:///{0}{1}", samplepath.FullName + @"\", page);
        }

        public abstract string GetSubDirectory { get; }

        public static string AssemblyDirectory
        {
            get
            {
                string codeBase = System.Reflection.Assembly.GetExecutingAssembly().CodeBase;
                UriBuilder uri = new UriBuilder(codeBase);
                string path = Uri.UnescapeDataString(uri.Path);
                return Path.GetDirectoryName(path);

            }
        }

        public virtual Bitmap UISnap(Rectangle clipBounds, OpenQA.Selenium.IWebDriver webDriver, IWebElement element)
        {

            //Manager.Current.ActiveBrowser.MessageFlush();
            System.Threading.Thread.Sleep(1000);


            OpenQA.Selenium.Screenshot ss = ((OpenQA.Selenium.ITakesScreenshot)webDriver).GetScreenshot();
            using (Bitmap pageBmp = new Bitmap(new System.IO.MemoryStream(ss.AsByteArray)))
            {
                //Debug to see page image.
                //pageBmp.Save(@"D:\PageImage.bmp");
                Rectangle bounds = new Rectangle(element.Location.X, element.Location.Y, element.Size.Width, element.Size.Height);    //this.GetDomElementBounds();
                int x = bounds.X;
                int y = bounds.Y;

                ////Chrome and IE only snap browser client bounds, so need consider browser scroll offset...
                //if (Manager.Current.ActiveBrowser.BrowserType != BrowserType.Firefox)
                //{
                //    x = x - this.DocumentScrollLeft;
                //    y = y - this.DocumentScrollTop;
                //}

                //if (Manager.Current.ActiveBrowser.BrowserType == BrowserType.IE)
                //{
                //    x += 1;
                //}

                Rectangle elementRect = new Rectangle(new Point(x, y), bounds.Size);
                using (Bitmap elementBmp = pageBmp.Clone(Rectangle.Intersect(elementRect, new Rectangle(Point.Empty, pageBmp.Size)), PixelFormat.DontCare))
                {
                    //Rectangle actualClipBounds = Rectangle.Intersect(new Rectangle(Point.Empty, elementRect.Size), bounds);
                    //if (clipBounds != Rectangle.Empty)
                    //{
                    //    actualClipBounds = Rectangle.Intersect(actualClipBounds, clipBounds);
                    //}
                    return elementBmp.Clone(new Rectangle(Point.Empty, elementBmp.Size),
                        PixelFormat.DontCare);
                    // return elementBmp;
                }
            }
        }

        public virtual void CheckUIAppearance(System.Drawing.Bitmap bmp, string label = null, bool isExactMatch = false)
        {
            if (!AssemblyInitConfige.DoUICheckAppearance)
            {
                return;
            }

            var snapshot = Snapshot.FromBitmap(bmp);
            string newLabel = ("Chrome_" + label);
            bmp.Dispose();
            GrapeCity.Testing.Tools.Framework.Assertion.CheckUIAppearance(snapshot, newLabel, null, isExactMatch);
        }

    }

    [TestClass]
    public sealed class AssemblyInitConfige
    {
        internal static bool DoUICheckAppearance = true;
        [AssemblyInitialize()]
        public static void AssemblyInit(TestContext context)
        {
            //DoUICheckAppearance = Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings["UICheckAppearanceEnable"]);
            //UIAppearanceSettings.Instance.ServerName = @"xa-qa-sql";
            //UIAppearanceSettings.Instance.Password = @"";
            UIAppearanceSettings.Instance.ServerName = @"IN-ATW-WIN2016\SQL_2019";
            UIAppearanceSettings.Instance.Password = @"grapecity@123";
            UIAppearanceSettings.Instance.TestProject = @"Wijimo";
            UIAppearanceSettings.Instance.UserName = @"sa";
            UIAppearanceSettings.Instance.IgnoreEnvironmentInfo = true;
        }
    }
}
