﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Threading;
using Selenium.Test.Toolkit.Action;
using Selenium.Test.Toolkit.GUI;
using OpenQA.Selenium;
using OpenQA.Selenium.Remote;

namespace C1Wijmo5.TestScripts
{
    /// <summary>
    /// Summary description for UnitTestDragdrop
    /// </summary>
    [TestClass]
    public class UnitTestDragdrop : Selenium.Test.Toolkit.Test.BaseTest
    {
        public UnitTestDragdrop()
        {
            //
            // TODO: Add constructor logic here
            //
        }


        private TestContext testContextInstance;
        [TestMethod]
        public void TestMethodForOutSystem()
        {
            Selenium.Test.Toolkit.Core.Manager.IgnoreAssertAlertDialog = true;
            Selenium.Test.Toolkit.Core.Manager.Current.TestSettings.ActionType = ActionType.Windows;
        
            this.ActiveBrowser.GoToUrl("https://silko11qa.outsystems.net/Test_DataGridSample_Stable/GridConfigs.aspx");

            Thread.Sleep(10000);


            var selectedColumnforGroup = this.ActiveBrowser.FindDisplayedElementGUIs<DomElementGUI>(By.CssSelector(".wj-colheaders .wj-header")).FirstOrDefault((el) => el.TextContent.Contains("Download"));

            //Get the GroupPanel
            var panel = this.ActiveBrowser.FindDisplayedElementGUI(By.CssSelector(".wj-grouppanel div"));

            //Drag Drop using WinAPI to GroupPanel
            selectedColumnforGroup.UIDragTo(panel, true);

            //Get the Group rows count after applying grouping
            var rows = this.ActiveBrowser.FindDisplayedElementGUIs<DomElementGUI>(By.CssSelector(".wj-cells .wj-row .wj-group"));

            Assert.IsTrue(rows.Count > 0);

        }

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion

        [TestMethod]
        public void TestMethod1()
        {
            //
            // TODO: Add test logic here
            //
        }
    }
}
