﻿using C1Wijmo5.TestScripts.Extend;
using C1Wijmo5.TestScripts.Extention;
using C1Wijmo5.TestScripts.Helpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Remote;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading;
using static C1Wijmo5.TestScripts.Helpers.FlexGridTestHelper;

namespace C1Wijmo5.TestScripts
{
    [TestClass]
    public class Test_FlexGrid : BaseTest
    {
        public override string GetSubDirectory => "FlexGrid";
        Helpers.FlexGridTestHelper gridHelper = new Helpers.FlexGridTestHelper();

        #region TC_FlexGrid_82985_Numeric

        [TestMethod]
        [TestCategory("Grouping")]
        [Description("82985,,CP=35%,Checking grouping can do on various column type,To test whether Grouping can be done on various column type[ String, Numeric, Boolean and Date]")]
        public void TC_FlexGrid_82985_Numeric()
        {
            C.Execute_Test("TC_FlexGrid_82985_Numeric_Test");
        }

        public void TC_FlexGrid_82985_Numeric_Test(RemoteWebDriver browser)
        {
            browser.Manage().Window.Maximize();
            try
            {
            
                browser.Navigate().GoToUrl("https://silko11qa.outsystems.net/Test_DataGridSample_Stable/Grid.aspx");
                Thread.Sleep(300);
                GridJsHelper gridJsHelper = new GridJsHelper(browser);
                bool canApplyGroupingOnNumberTypes = gridJsHelper.CheckIfDataTypeCouldBeGrouped(browser, "Number");
                Assert.AreEqual(canApplyGroupingOnNumberTypes, true);
            }
            finally
            {
                browser.Quit();
            }
        }

        #endregion TC_FlexGrid_82985_Numeric

        #region TC_FlexGrid_82985_String

        [TestMethod]
        [TestCategory("Grouping")]
        [Description("82985,,CP=35%,Checking grouping can do on various column type,To test whether Grouping can be done on various column type[ String, Numeric, Boolean and Date]")]
        public void TC_FlexGrid_82985_String()
        {
            C.Execute_Test("TC_FlexGrid_82985_String_Test");
        }

        public void TC_FlexGrid_82985_String_Test(RemoteWebDriver browser)
        {
            browser.Manage().Window.Maximize();
            try
            {
                //browser.Navigate().GoToUrl(Get_URL("TC_114471.htm"));
                browser.Navigate().GoToUrl("https://silko11qa.outsystems.net/Test_DataGridSample_Stable/Grid.aspx");
                Thread.Sleep(300);


                GridJsHelper gridJsHelper = new GridJsHelper(browser);
              
                bool canApplyGroupingOnStringTypes = gridJsHelper.CheckIfDataTypeCouldBeGrouped(browser, "String");
                Assert.AreEqual(canApplyGroupingOnStringTypes, true);
            }
            finally
            {
                browser.Quit();
            }
        }

        #endregion TC_FlexGrid_82985_String

        #region TC_FlexGrid_82985_Date

        [TestMethod]
        [TestCategory("Grouping")]
        [Description("82985,,CP=35%,Checking grouping can do on various column type,To test whether Grouping can be done on various column type[ String, Numeric, Boolean and Date]")]
        public void TC_FlexGrid_82985_Date()
        {
            C.Execute_Test("TC_FlexGrid_82985_Date_Test");
        }

        public void TC_FlexGrid_82985_Date_Test(RemoteWebDriver browser)
        {
            browser.Manage().Window.Maximize();
            try
            {
                //browser.Navigate().GoToUrl(Get_URL("TC_114471.htm"));
                browser.Navigate().GoToUrl("https://silko11qa.outsystems.net/Test_DataGridSample_Stable/Grid.aspx");
                Thread.Sleep(300);
                GridJsHelper gridJsHelper = new GridJsHelper(browser);
                bool canApplyGroupingOnDateTypes = gridJsHelper.CheckIfDataTypeCouldBeGrouped(browser, "Date");
                Assert.AreEqual(canApplyGroupingOnDateTypes, true);
            }
            finally
            {
                browser.Quit();
            }
        }

        #endregion TC_FlexGrid_82985_Date

        #region TC_FlexGrid_82985_Boolean

        [TestMethod]
        [TestCategory("Grouping")]
        [Description("82985,,CP=35%,Checking grouping can do on various column type,To test whether Grouping can be done on various column type[ String, Numeric, Boolean and Date]")]
        public void TC_FlexGrid_82985_Boolean()
        {
            C.Execute_Test("TC_FlexGrid_82985_Boolean_Test");
        }

        public void TC_FlexGrid_82985_Boolean_Test(RemoteWebDriver browser)
        {
            browser.Manage().Window.Maximize();
            try
            {
                //browser.Navigate().GoToUrl(Get_URL("TC_114471.htm"));
                browser.Navigate().GoToUrl("https://silko11qa.outsystems.net/Test_DataGridSample_Stable/Grid.aspx");
                Thread.Sleep(300);
                GridJsHelper gridJsHelper = new GridJsHelper(browser);
                bool canApplyGroupingOnDateTypes = gridJsHelper.CheckIfDataTypeCouldBeGrouped(browser, "Boolean");
                Assert.AreEqual(canApplyGroupingOnDateTypes, true);
            }
            finally
            {
                browser.Quit();
            }
        }

        #endregion TC_FlexGrid_82985_Boolean

        #region TC_FlexGrid_Sort_Ascending

        [TestMethod]
        [TestCategory("Sorting")]
        [Description("To check if sorting can be done in ascending order")]
        public void TC_FlexGrid_Sort_Ascending()
        {
            C.Execute_Test("TC_FlexGrid_Sort_Ascending_Test");
        }

        public void TC_FlexGrid_Sort_Ascending_Test(RemoteWebDriver browser)
        {
            browser.Manage().Window.Maximize();
            try
            {
                //browser.Navigate().GoToUrl(Get_URL("TC_114471.htm"));
                browser.Navigate().GoToUrl("https://silko11qa.outsystems.net/Test_DataGridSample_Stable/Grid.aspx");
                Thread.Sleep(300);

                // get grid object
                browser.ExecuteScript($"window['gridObj'] = GridOS.ComponentUtils.getGridObjectById()");

                // clear any existing sort
                browser.ExecuteScript($"gridObj.grid.collectionView.sortDescriptions.clear()");

                // click on col header to sort
                var headerEl = browser.FindElement(By.CssSelector(".wj-flexgrid .wj-colheaders .wj-cell.wj-header"));
                var headerText = headerEl.Text;
                headerEl.Click();
                Thread.Sleep(300);
                // check if sort icon is present
                var el = browser.FindElement(By.CssSelector(".wj-flexgrid .wj-colheaders .wj-cell.wj-header.wj-sort-asc"));
                bool isSorted = el != null && el.Text.Trim() == headerText.Trim();

                Assert.AreEqual(true, isSorted);
            }
            finally
            {
                browser.Quit();
            }
        }

        #endregion TC_FlexGrid_Sort_Ascending

        #region TC_FlexGrid_CheckboxSelection_AllChecked

        [TestMethod]
        [TestCategory("CheckboxSelection")]
        [Description("To check if all rows are selected if top-left cell checkbox is checked")]
        public void TC_FlexGrid_CheckboxSelection_AllChecked()
        {
            C.Execute_Test("TC_FlexGrid_CheckboxSelection_AllChecked_Test");
        }

        public void TC_FlexGrid_CheckboxSelection_AllChecked_Test(RemoteWebDriver browser)
        {
            browser.Manage().Window.Maximize();
            try
            {
                //browser.Navigate().GoToUrl(Get_URL("TC_114471.htm"));
                browser.Navigate().GoToUrl("https://silko11qa.outsystems.net/Test_DataGridSample_Stable/Grid.aspx");
                Thread.Sleep(300);

                // get grid object
                browser.ExecuteScript($"window['gridObj'] = GridOS.ComponentUtils.getGridObjectById()");
                var headercb = browser.FindElement(By.CssSelector(".wj-flexgrid .wj-topleft [type='checkbox'].select-all"));
                if (!headercb.Selected)
                {
                    headercb.Click();
                    Thread.Sleep(300);
                }

                // actual no of rows
                long numRows = (long)browser.ExecuteScript($"return gridObj.grid.rows.length");
                // selected rows
                long selRows = (long)browser.ExecuteScript($"return GridOS.SelectionFeature.getSelectedRows()");

                Assert.AreEqual(numRows, selRows);
            }
            finally
            {
                browser.Quit();
            }
        }

        #endregion TC_FlexGrid_CheckboxSelection_AllChecked

        #region TC_FlexGrid_CheckboxSelection_AllUnChecked

        [TestMethod]
        [TestCategory("CheckboxSelection")]
        [Description("To check if all rows are unselected if top-left cell checkbox is unchecked")]
        public void TC_FlexGrid_CheckboxSelection_AllUnChecked()
        {
            C.Execute_Test("TC_FlexGrid_CheckboxSelection_AllUnChecked_Test");
        }

        public void TC_FlexGrid_CheckboxSelection_AllUnChecked_Test(RemoteWebDriver browser)
        {
            browser.Manage().Window.Maximize();
            try
            {
                //browser.Navigate().GoToUrl(Get_URL("TC_114471.htm"));
                browser.Navigate().GoToUrl("https://silko11qa.outsystems.net/Test_DataGridSample_Stable/Grid.aspx");
                Thread.Sleep(300);

                // get grid object
                browser.ExecuteScript($"window['gridObj'] = GridOS.ComponentUtils.getGridObjectById()");
                var headercb = browser.FindElement(By.CssSelector(".wj-flexgrid .wj-topleft [type='checkbox'].select-all"));
                if (!headercb.Selected)
                {
                    headercb.Click();
                    Thread.Sleep(300);
                }

                headercb = browser.FindElement(By.CssSelector(".wj-flexgrid .wj-topleft [type='checkbox'].select-all"));
                headercb.Click();
                Thread.Sleep(300);

                long selRows = (long)browser.ExecuteScript($"return GridOS.SelectionFeature.getSelectedRows()");

                Assert.AreEqual(0, selRows);
            }
            finally
            {
                browser.Quit();
            }
        }

        #endregion TC_FlexGrid_CheckboxSelection_AllUnChecked

        #region TC_TestAutoGenerateColumnsProperty

        [TestMethod]
        [TestCategory("AutoGenerateColumns")]
        [Description("To test if columns are generated automatically after setting AutoGenerateColumns property to true")]
        public void TC_TestAutoGenerateColumnsProperty()
        {
            C.Execute_Test("TC_TestAutoGenerateColumnsProperty_Test");
        }

        public void TC_TestAutoGenerateColumnsProperty_Test(RemoteWebDriver browser)
        {
            browser.Manage().Window.Maximize();
            try
            {
                browser.Navigate().GoToUrl("https://silko11qa.outsystems.net/Test_DataGridSample_Stable/AutoGenerate.aspx");
                Thread.Sleep(300);

                // make sure autogenerate columns is enabled
                var cb = browser.FindElementByCssSelector($".checkbox");
                if (!cb.Selected)
                {
                    cb.Click();
                    Thread.Sleep(300);
                }

                // get grid object
                browser.ExecuteScript($"window['gridObj'] = GridOS.ComponentUtils.getGridObjectById()");

                // get generated columns binding + header
                string generatedColumnsBindings = (string)browser.ExecuteScript($"let columns = gridObj.grid.columns.map(c => c.binding); return columns.join('')");

                Thread.Sleep(500);
                // get expected columns binding + header
                string expectedColumns = (string)browser.ExecuteScript($"let dataItem = gridObj.grid.collectionView.items[0]; return Object.keys(dataItem).join('')");

                Assert.AreEqual(expectedColumns, generatedColumnsBindings);
            }
            finally
            {
                browser.Quit();
            }
        }

        #endregion TC_TestAutoGenerateColumnsProperty

        #region TC_TestAutoGenerateColumnsPropertyFalse

        [TestMethod]
        [TestCategory("AutoGenerateColumns")]
        [Description("To test if columns are generated according to col def after setting AutoGenerateColumns property to false")]
        public void TC_TestAutoGenerateColumnsPropertyFalse()
        {
            C.Execute_Test("TC_TestAutoGenerateColumnsPropertyFalse_Test");
        }

        public void TC_TestAutoGenerateColumnsPropertyFalse_Test(RemoteWebDriver browser)
        {
            browser.Manage().Window.Maximize();
            try
            {
                browser.Navigate().GoToUrl("https://silko11qa.outsystems.net/Test_DataGridSample_Stable/AutoGenerate.aspx");
                Thread.Sleep(300);

                // make sure autogenerate columns is disabled
                var cb = browser.FindElementByCssSelector($".checkbox.togglecheckbox");
                if (cb.Selected)
                {
                    cb.Click();
                    Thread.Sleep(300);
                }

                // get grid object
                browser.ExecuteScript($"window['gridObj'] = GridOS.ComponentUtils.getGridObjectById()");

                // get generated columns binding + header
                string generatedColumns = (string)browser.ExecuteScript($"let columns = gridObj.grid.columns.map(c => c.binding + c.header); return columns.join('')");

                Thread.Sleep(500);
                // get expected columns binding + header
                string expectedColumns = (string)browser.ExecuteScript($"let eCols = columnsDef.map(c => c.binding + c.header); return eCols.join('')");

                Assert.AreEqual(expectedColumns, generatedColumns);
            }
            finally
            {
                browser.Quit();
            }
        }

        #endregion TC_TestAutoGenerateColumnsPropertyFalse

        #region TC_SelectCellInModeCellRange

        [TestMethod]
        [TestCategory("AutoGenerateColumns")]
        [Description("To test if cell is selected correctly in cell range mode")]
        public void TC_SelectCellInModeCellRange()
        {
            C.Execute_Test("TC_SelectCellInModeCellRange_Test");
        }

        public void TC_SelectCellInModeCellRange_Test(RemoteWebDriver browser)
        {
            browser.Manage().Window.Maximize();
            try
            {
                browser.Navigate().GoToUrl("https://silko11qa.outsystems.net/Test_DataGridSample_Stable/AutoGenerate.aspx");
                Thread.Sleep(300);

                // get grid object
                browser.ExecuteScript($"window['gridObj'] = GridOS.ComponentUtils.getGridObjectById()");

                // set selection mode to cellrange
                browser.ExecuteScript($"gridObj.grid.selectionMode = wijmo.grid.SelectionMode.CellRange");
                Thread.Sleep(200);

                WjFlexGrid grid = new WjFlexGrid(browser);

                var r1c1 = grid.GetCell(2, 1);
                var r3c3 = grid.GetCell(4, 3);

                // drag mouse from cell 1:1 to 3:3 to select
                var action = new Actions(browser);
                action.MoveToElement(r1c1);
                action.ClickAndHold(r1c1);
                action.MoveToElement(r3c3);
                action.Release(r3c3);
                action.Build();
                action.Perform();

                // get current selection
                var sel = browser.ExecuteScript($"let sel = gridObj.grid.selection; return '' + sel.row2 + ',' + sel.col2 + ':' + sel.row + ',' + sel.col");

                Assert.AreEqual(sel, "1,1:3,3");
            }
            finally
            {
                browser.Quit();
            }
        }

        #endregion TC_SelectCellInModeCellRange

        #region TC_SelectCellInModeCell

        [TestMethod]
        [TestCategory("AutoGenerateColumns")]
        [Description("To test if cell is selected correctly in cell range mode")]
        public void TC_SelectCellInModeCell()
        {
            C.Execute_Test("TC_SelectCellInModeCell_Test");
        }

        public void TC_SelectCellInModeCell_Test(RemoteWebDriver browser)
        {
            browser.Manage().Window.Maximize();
            try
            {
                browser.Navigate().GoToUrl("https://silko11qa.outsystems.net/Test_DataGridSample_Stable/AutoGenerate.aspx");
                Thread.Sleep(300);

                // get grid object
                browser.ExecuteScript($"window['gridObj'] = GridOS.ComponentUtils.getGridObjectById()");

                // set selection mode to cellrange
                browser.ExecuteScript($"gridObj.grid.selectionMode = wijmo.grid.SelectionMode.Cell");
                Thread.Sleep(200);

                WjFlexGrid grid = new WjFlexGrid(browser);

                var r1c1 = grid.GetCell(2, 1);
                var r3c3 = grid.GetCell(4, 3);

                // drag mouse from cell 1:1 to 3:3 to select
                var action = new Actions(browser);
                action.MoveToElement(r1c1);
                action.ClickAndHold(r1c1);
                action.MoveToElement(r3c3);
                action.Release(r3c3);
                action.Build();
                action.Perform();

                // get current selection
                var sel = browser.ExecuteScript($"let sel = gridObj.grid.selection; return '' + sel.row2 + ',' + sel.col2 + ':' + sel.row + ',' + sel.col");

                Assert.AreEqual(sel, "3,3:3,3");
            }
            finally
            {
                browser.Quit();
            }
        }

        #endregion TC_SelectCellInModeCell

        #region TC_SelectCellInModeRow

        [TestMethod]
        [TestCategory("AutoGenerateColumns")]
        [Description("To test if cell is selected correctly in cell range mode")]
        public void TC_SelectCellInModeRow()
        {
            C.Execute_Test("TC_SelectCellInModeRow_Test");
        }

        public void TC_SelectCellInModeRow_Test(RemoteWebDriver browser)
        {
            browser.Manage().Window.Maximize();
            try
            {
                browser.Navigate().GoToUrl("https://silko11qa.outsystems.net/Test_DataGridSample_Stable/AutoGenerate.aspx");
                Thread.Sleep(300);

                // get grid object
                browser.ExecuteScript($"window['gridObj'] = GridOS.ComponentUtils.getGridObjectById()");

                // set selection mode to cellrange
                browser.ExecuteScript($"gridObj.grid.selectionMode = wijmo.grid.SelectionMode.Row");
                Thread.Sleep(200);

                WjFlexGrid grid = new WjFlexGrid(browser);

                var r1c1 = grid.GetCell(2, 1);
                var r3c3 = grid.GetCell(4, 3);

                // drag mouse from cell 1:1 to 3:3 to select
                var action = new Actions(browser);
                action.MoveToElement(r1c1);
                action.ClickAndHold(r1c1);
                action.MoveToElement(r3c3);
                action.Release(r3c3);
                action.Build();
                action.Perform();
                // check if only single row is selected
                string sel = (string)browser.ExecuteScript($"let sel = gridObj.grid.selection; return '' + sel.row + ':' + sel.row2");

                // check if all cells in the row are selected
                var cells = browser.FindElement(By.CssSelector(".wj-flexgrid .wj-cells")).FindElements(By.ClassName("wj-row"))[4].FindElements(By.ClassName("wj-cell"));
                var allSel = cells.All(el => el.GetAttribute("class").Contains("wj-state-selected") || el.GetAttribute("class").Contains("wj-state-multi-selected"));

                string value = sel + (allSel ? "true" : "false");

                Assert.AreEqual("3:3true", value);
            }
            finally
            {
                browser.Quit();
            }
        }

        #endregion TC_SelectCellInModeRow

        #region Test_UniqueValues

        [TestMethod]
        [TestCategory("FlexGrid")]
        [Description("To test whether the uniqueValues property properly assigns the array as filter values")]
        public void TC_UniqeValues()
        {
            C.Execute_Test("TC_UniqeValues_Test");
        }

        public void TC_UniqeValues_Test(RemoteWebDriver browser)
        {
            browser.Manage().Window.Maximize();

            try
            {
                browser.Navigate().GoToUrl("https://silko11qa.outsystems.net/Test_DataGridSample_Stable/GridFilter.aspx?FilterTypeId=Both");

                IWebElement countryFilter = browser.FindElementByClassName("wj-colheaders").FindElements(By.ClassName("wj-elem-filter"))[1];
                countryFilter.Click();

                Thread.Sleep(300);

                //Get the Grid Filter
                browser.ExecuteScript("window['gridFilter'] = wijmo.Control.getControl('.wj-columnfiltereditor')");

                //Get Unique values from the value filter
                var valuesList = ((ReadOnlyCollection<object>)browser.ExecuteScript("return window['gridFilter'].filter.valueFilter.uniqueValues")).ToList();

                valuesList.Sort();

                // select all is always shown
                string expectedList = "Select All";

                foreach (object val in valuesList)
                {
                    expectedList += val.ToString();
                }

                var listItems = browser.FindElements(By.ClassName("wj-listbox-item"));

                string actualListText = "";

                foreach (IWebElement listItem in listItems)
                {
                    actualListText += listItem.Text;
                }

                Assert.AreEqual(expectedList, actualListText);

                Thread.Sleep(1000);
            }
            finally
            {
                browser.Quit();
            }
        }

        #endregion Test_UniqueValues

        #region Test_LoadEvents

        [TestMethod]
        [TestCategory("FlexGrid")]
        [Description("Test whether loadingRows and loadedRows event is called after FlexGrid is bound to the data")]
        public void TC_LoadRows()
        {
            C.Execute_Test("TC_LoadRows_Test");
        }

        public void TC_LoadRows_Test(RemoteWebDriver browser)
        {
            //** Must be added in code in Sample fro test to work***//
            //grid.loadedRows.addHandler(function() {
            //    window["afterLoad"] = grid.rows.length;
            //    div.innerHTML += 'rowsloaded';
            //});
            //grid.loadingRows.addHandler(function() {
            //    window["beforeLoad"] = grid.rows.length;
            //    div.innerHTML += 'rowsloading';
            //});
            browser.Manage().Window.Maximize();
            try
            {
                browser.Navigate().GoToUrl("https://silko11qa.outsystems.net/Test_DataGridSample_Stable/GridFilter.aspx?FilterTypeId=Both");
                Thread.Sleep(500);
                //Get
                long before = (long)browser.ExecuteScript("return window['beforeLoad']");
                long after = (long)browser.ExecuteScript("return window['afterLoad']");

                browser.ExecuteScript("window['grid'] = wijmo.Control.getControl('.wj-flexgrid')");

                long total = (long)browser.ExecuteScript("return window['grid'].collectionView.items.length");

                string actualText = string.Format("beforeLoad:{0}|afterLoad:{1}", before, after);

                string expectedText = string.Format("beforeLoad:{0}|afterLoad:{1}", 0, total);

                //string
                Assert.AreEqual(expectedText, actualText);
                Thread.Sleep(1000);
            }
            finally
            {
                browser.Quit();
            }
        }

        #endregion Test_LoadEvents

        #region Test_AlternatingRows

        [TestMethod]
        [TestCategory("FlexGrid")]
        [Description("To test whether numbers of regular rows between alternate rows can be set correctly or not by using 'alternatingRowStep' property")]
        public void TC_AlternateRows()
        {
            C.Execute_Test("TC_AlternateRows_Test");
        }

        public void TC_AlternateRows_Test(RemoteWebDriver browser)
        {
            browser.Manage().Window.Maximize();
            try
            {
                //** Must be added in code in Sample for test to work***//
                //grid.alternatingRowStep = 4;

                browser.Navigate().GoToUrl("https://silko11qa.outsystems.net/Test_DataGridSample_Stable/Grid.aspx?FilterTypeId=Both");

                Thread.Sleep(300);

                browser.ExecuteScript("window['gridObj'] = wijmo.Control.getControl('.wj-flexgrid')");

                //Get the alternatingrow step
                int rowStep = Convert.ToInt32(browser.ExecuteScript("return window['gridObj'].alternatingRowStep"));

                var rows = browser.FindElements(By.CssSelector(".wj-cells .wj-row"));

                bool check = true;
                int nextAltRow = 1;

                // start row from 1
                // because first row is for headers
                // and not for cells
                for (int row = 1; row < rows.Count; row++)
                {
                    if (row == nextAltRow)
                    {
                        nextAltRow += rowStep + 1;
                        continue;
                    }

                    // When Alternating Step is applied, these rows will not have .wj-alt class
                    var cells = rows[row].FindElements(By.CssSelector(".wj-alt"));
                    check &= cells.Count == 0;
                }

                Assert.IsTrue(check);
            }
            finally
            {
                browser.Quit();
            }
        }

        #endregion Test_AlternatingRows

        #region TC_AutoSizeAutoColumn

        [TestMethod]
        [Description("Auto-size columns using methods,To test whether 'autoSizeColumn/autoSizeColumns' methods are working correctly in FlexGrid")]
        public void TC_AutoSizeAutoColumn()
        {
            C.Execute_Test("TC_AutoSizeAutoColumn_Test");
        }

        public void TC_AutoSizeAutoColumn_Test(RemoteWebDriver browser)
        {
            browser.Manage().Window.Maximize();
            try
            {
                browser.Navigate().GoToUrl("https://silko11qa.outsystems.net/Test_DataGridSample_Stable/GridFilter.aspx?FilterTypeId=Both");
                Thread.Sleep(300);
                // Get FlexGrid id from OutSystems datagrid
                string flexGridId = Convert.ToString(browser.ExecuteScript("return wijmo.Control.getControl('.wj-flexgrid').hostElement.id"));

                //Click 1st 'autoSizeColumn' button
                browser.FindElementById("btnAutoSizeCol").Click();
                Thread.Sleep(100);

                //Get the width of auto-sized column

                var cells = browser.FindElementById(flexGridId).FindElement(By.ClassName("wj-cells"));
                var msg = cells.FindElements(By.ClassName("wj-row"))[1].FindElements(By.ClassName("wj-cell"))[0].GetCssValue("width") + ",";

                //Click 2nd 'autoSizeColumn' button
                browser.FindElementById("btnAutoSizeCol2").Click();
                Thread.Sleep(100);
                msg += "," + browser.FindElementById(flexGridId).FindElement(By.ClassName("wj-rowheaders")).FindElements(By.ClassName("wj-row"))[1].FindElement(By.ClassName("wj-header")).GetCssValue("width");

                //Click 3rd 'autoSizeColumn' button
                browser.FindElementById("btnAutoSizeCol3").Click();
                Thread.Sleep(100);
                msg += "," + cells.FindElements(By.ClassName("wj-row"))[2].FindElements(By.ClassName("wj-cell"))[1].GetCssValue("width");

                //Click 4th 'autoSizeColumn' button
                browser.FindElementById("btnAutoSizeCol4").Click();
                Thread.Sleep(100);
                msg += "," + cells.FindElements(By.ClassName("wj-row"))[2].FindElements(By.ClassName("wj-cell"))[1].GetCssValue("width");

                if (C.IsIE(browser))
                    AssertEx.AreEqual("18px,32px,64px,70px", msg, browser.GetType().Name);
                else
                    AssertEx.AreEqual("32px,45px,77px,83px", msg, browser.GetType().Name);
            }
            finally
            {
                browser.Quit();
            }
        }

        #endregion TC_AutoSizeAutoColumn

        #region TC_AutoSizeAutoColumns

        [TestMethod]
        [Description("Auto-size columns using methods,To test whether 'autoSizeColumn/autoSizeColumns' methods are working correctly in FlexGrid")]
        public void TC_AutoSizeAutoColumns()
        {
            C.Execute_Test("TC_AutoSizeAutoColumns_Test");
        }

        public void TC_AutoSizeAutoColumns_Test(RemoteWebDriver browser)
        {
            browser.Manage().Window.Maximize();
            try
            {
                browser.Navigate().GoToUrl("https://silko11qa.outsystems.net/Test_DataGridSample_Stable/GridFilter.aspx?FilterTypeId=Both");
                Thread.Sleep(300);
                // Get FlexGrid id from OutSystems datagrid
                string flexGridId = Convert.ToString(browser.ExecuteScript("return wijmo.Control.getControl('.wj-flexgrid').hostElement.id"));

                var cells = browser.FindElementById(flexGridId).FindElement(By.ClassName("wj-cells"));
                var msg = cells.FindElements(By.ClassName("wj-row"))[1].FindElements(By.ClassName("wj-cell"))[0].GetCssValue("width") + ",";

                //Click 1st 'autoSizeColumns' button
                browser.FindElementById("btnAutoSizeCols").Click();
                Thread.Sleep(100);
                //Get the width of auto-sized columns
                cells = browser.FindElementById(flexGridId).FindElement(By.ClassName("wj-cells"));
                msg = cells.FindElements(By.ClassName("wj-row"))[1].FindElements(By.ClassName("wj-cell"))[0].GetCssValue("width");
                msg += "," + cells.FindElements(By.ClassName("wj-row"))[1].FindElements(By.ClassName("wj-cell"))[1].GetCssValue("width");

                //Click 2nd 'autoSizeColumns' button
                browser.FindElementById("btnAutoSizeCols2").Click();
                Thread.Sleep(100);
                msg += "|" + cells.FindElements(By.ClassName("wj-row"))[1].FindElements(By.ClassName("wj-cell"))[2].GetCssValue("width");
                msg += "," + cells.FindElements(By.ClassName("wj-row"))[1].FindElements(By.ClassName("wj-cell"))[3].GetCssValue("width");
                msg += "," + cells.FindElements(By.ClassName("wj-row"))[1].FindElements(By.ClassName("wj-cell"))[4].GetCssValue("width");

                //Click 3rd 'autoSizeColumns' button
                browser.FindElementById("btnAutoSizeCols3").Click();
                Thread.Sleep(100);
                msg += "|" + cells.FindElements(By.ClassName("wj-row"))[1].FindElements(By.ClassName("wj-cell"))[2].GetCssValue("width");
                msg += "," + cells.FindElements(By.ClassName("wj-row"))[1].FindElements(By.ClassName("wj-cell"))[3].GetCssValue("width");
                msg += "," + cells.FindElements(By.ClassName("wj-row"))[1].FindElements(By.ClassName("wj-cell"))[4].GetCssValue("width");

                //Click 4th 'autoSizeColumns' button
                browser.FindElementById("btnAutoSizeCols4").Click();
                Thread.Sleep(100);
                msg += "|" + cells.FindElements(By.ClassName("wj-row"))[1].FindElements(By.ClassName("wj-cell"))[0].GetCssValue("width");
                msg += "," + cells.FindElements(By.ClassName("wj-row"))[1].FindElements(By.ClassName("wj-cell"))[1].GetCssValue("width");
                msg += "," + cells.FindElements(By.ClassName("wj-row"))[1].FindElements(By.ClassName("wj-cell"))[2].GetCssValue("width");
                msg += "," + cells.FindElements(By.ClassName("wj-row"))[1].FindElements(By.ClassName("wj-cell"))[3].GetCssValue("width");
                msg += "," + cells.FindElements(By.ClassName("wj-row"))[1].FindElements(By.ClassName("wj-cell"))[4].GetCssValue("width");

                if (C.IsIE(browser))
                    AssertEx.AreEqual("18px,64px|76px,57px,46px|92px,73px,62px|18px,64px,76px,57px,46px", msg, browser.GetType().Name);
                else if (C.IsFireFox(browser))
                    AssertEx.AreEqual("32px,77px|91px,71px,62px|107px,87px,78px|32px,77px,91px,71px,62px", msg, browser.GetType().Name);
                else
                    AssertEx.AreEqual("32px,77px|91px,71px,62px|107px,87px,78px|32px,77px,91px,71px,62px", msg, browser.GetType().Name);
            }
            finally
            {
                browser.Quit();
            }
        }

        #endregion TC_AutoSizeAutoColumns


        #region Test_SortScrollVertical

        [TestMethod]
        [TestCategory("FlexGrid")]
        [Description("To test whether sort works correctly after scrolling the grid vertically")]
        public void TC_ScrollSortVertical()
        {
            C.Execute_Test("TC_ScrollSortVertical_Test");
        }

        public void TC_ScrollSortVertical_Test(RemoteWebDriver browser)
        {
            browser.Manage().Window.Maximize();

            try
            {
                browser.Navigate().GoToUrl("https://silko11qa.outsystems.net/Test_DataGridSample_Stable/Grid.aspx");

                Thread.Sleep(300);

                // create grid object
                browser.ExecuteScript("window['grid'] = wijmo.Control.getControl('.wj-flexgrid')");

                // get total number of columns
                int totalRows = int.Parse(browser.ExecuteScript("return window['grid'].rows.length").ToString());

                // get the binding of first column
                string binding = browser.ExecuteScript("return window['grid'].columns[0].binding").ToString();

                // get the header of the flexgrid
                IWebElement header = browser.FindElementsByCssSelector(".wj-colheaders .wj-row .wj-header.wj-cell")[0];
                // click on the header
                header.Click();

                // get the items before scrolling
                var originalItems = gridHelper.GetDataItems(browser);

                Thread.Sleep(300);

                // scroll the grid horizontally
                browser.ExecuteScript("window['grid'].scrollIntoView(arguments[0], -1);", totalRows - 1);

                Thread.Sleep(500);

                // get items after scrolling
                var newItems = gridHelper.GetDataItems(browser);

                // create a comparer
                IEqualityComparer<Dictionary<string, object>> comparer = new ItemsComparer
                {
                    Binding = binding
                };

                // check if sequence is same
                bool isSorted = originalItems.SequenceEqual(newItems, comparer);

                // assert the result
                Assert.IsTrue(isSorted);

                Thread.Sleep(2000);
            }
            finally
            {
                browser.Quit();
            }
        }
        #endregion

        #region Test_SortScrollHorizontal

        [TestMethod]
        [TestCategory("FlexGrid")]
        [Description("To test whether sort works correctly after scrolling the grid horizontally")]
        public void TC_ScrollSortHorizontal()
        {
            C.Execute_Test("TC_ScrollSortHorizontal_Test");
        }

        public void TC_ScrollSortHorizontal_Test(RemoteWebDriver browser)
        {
            browser.Manage().Window.Maximize();

            try
            {
                browser.Navigate().GoToUrl("https://silko11qa.outsystems.net/Test_DataGridSample_Stable/Grid.aspx");

                Thread.Sleep(300);

                // create grid object
                browser.ExecuteScript("window['grid'] = wijmo.Control.getControl('.wj-flexgrid')");

                // get total number of columns
                int totalCols = int.Parse(browser.ExecuteScript("return window['grid'].columns.length").ToString());

                // get the binding of first column
                string binding = browser.ExecuteScript("return window['grid'].columns[0].binding").ToString();

                // get the header of the flexgrid
                IWebElement header = browser.FindElementsByCssSelector(".wj-colheaders .wj-row .wj-header.wj-cell")[0];
                // click on the header
                header.Click();

                // get the items before scrolling
                var originalItems = gridHelper.GetDataItems(browser);

                Thread.Sleep(300);

                // scroll the grid horizontally
                browser.ExecuteScript("window['grid'].scrollIntoView(-1, arguments[0]);", totalCols - 1);

                Thread.Sleep(500);

                // get items after scrolling
                var newItems = gridHelper. GetDataItems(browser);

                // create a comparer
                IEqualityComparer<Dictionary<string, object>> comparer = new ItemsComparer
                {
                    Binding = binding
                };

                // check if sequence is same
                bool isSorted = originalItems.SequenceEqual(newItems, comparer);

                // assert the result
                Assert.IsTrue(isSorted);

                Thread.Sleep(2000);
            }
            finally
            {
                browser.Quit();
            }
        }

        #endregion

        #region Test_ParseDate

        [TestMethod]
        [TestCategory("FlexGrid")]
        [Description("To test whether the FlexGrid shows correct JSON dates after parsing using JSON.parse method")]
        public void TC_JSONDates()
        {
            C.Execute_Test("TC_JSONDates_Test");
        }

        public void TC_JSONDates_Test(RemoteWebDriver browser)
        {
            browser.Manage().Window.Maximize();

            try
            {
                browser.Navigate().GoToUrl("https://silko11qa.outsystems.net/Test_DataGridSample_Stable/Grid.aspx");

                Thread.Sleep(500);

                GridJsHelper helper = new GridJsHelper(browser);

                // get the first item in the collection view
                var firstItem = helper.GetDataItems().First();
                var columnBindings = helper.GetBindingColumns();

                string binding = "";

                // find a column containing date
                foreach (string key in columnBindings)
                {
                    // not visible column
                    if (!bool.Parse(helper.GetColumnProperty(key, "visible").ToString()))
                    {
                        continue;
                    }

                    object val = firstItem[key];
                    if (DateTime.TryParse(val.ToString(), out _))
                    {
                        binding = key;
                        break;
                    }
                }

                // if not found
                // then throw exception
                if (binding.Equals(""))
                {
                    throw new NotFoundException("A date type column not found");
                }

                // get the current column index
                int currentColumnIndex = int.Parse(helper.GetColumnProperty(binding, "index").ToString());

                // scroll to the column
                helper.ScrollIntoView(-1, currentColumnIndex);

                // find the rows
                var headerRow = browser.FindElementsByCssSelector(".wj-cells .wj-row");

                // find the index of the date column in the DOM
                int idx = helper.GetColumnDOMIndex(binding);

                string format = helper.GetColumnProperty(binding, "format").ToString();

                // get the formatted date
                string formattedDate = helper.GetFormattedDate(helper.GetItem(0, binding).ToString(), format);

                int firstDataIndex = helper.FindFirstDataRowIndex();

                // get the actual date shown
                string actualDate = headerRow[firstDataIndex].FindElements(By.CssSelector(".wj-cell"))[idx].Text;
                // assert them
                Assert.AreEqual(formattedDate, actualDate);
            }
            finally
            {
                browser.Quit();
            }
        }

        #endregion

        #region Test_FreezingWithMerging
        [TestMethod]
        [TestCategory("FlexGrid")]
        [Description("To test whether freezing is working correctly or not when there is merged columns in the grid")]
        public void TC_FreezingWithMerging()
        {
            C.Execute_Test("TC_FreezingWithMerging_Test");
        }
        public void TC_FreezingWithMerging_Test(RemoteWebDriver browser)
        {
            browser.Manage().Window.Maximize();
            try
            {
                browser.Navigate().GoToUrl(Get_URL("Merge/index.html"));
                Thread.Sleep(500);
                GridJsHelper gridJshelper = new GridJsHelper(browser);
                var cellRange = gridJshelper.GetSingleCellInfo();
                gridJshelper.ScrollIntoView(0, gridJshelper.GetColumnCount() - 1);
                Thread.Sleep(500);
                var cell = gridJshelper.GetCellElement("wj-cells", 0 + 1, 0);
                Assert.IsTrue(cell.GetAttribute("class").Split(' ').Contains("wj-frozen"));
            }
            finally
            {
                browser.Quit();
            }
        }
        #endregion


        #region Test_Collapse_Mouse

        [TestMethod]
        [TestCategory("FlexGrid")]
        [Description("To test whether the group rows can be collapsed using mouse")]
        public void TC_Collapse_Mouse()
        {
            C.Execute_Test("TC_Collapse_Mouse_Test");
        }

        public void TC_Collapse_Mouse_Test(RemoteWebDriver browser)
        {
            browser.Manage().Window.Maximize();

            try
            {
                browser.Navigate().GoToUrl("https://silko11qa.outsystems.net/Test_DataGridSample_Stable/Grid.aspx");
                Thread.Sleep(300);

                IWebElement firstGroupCell = browser.FindElementByCssSelector(".wj-flexgrid .wj-cells .wj-row .wj-group");
                IWebElement firstExpandButton = firstGroupCell.FindElement(By.CssSelector(".wj-elem-collapse"));
                IWebElement siblingGroupCell = firstGroupCell.FindElement(By.XPath("./..")).FindElement(By.XPath("following-sibling::*"));

                string res = "";

                GridJsHelper helper = new GridJsHelper(browser);

                // check if all childs are initially visible
                bool isAllChildVisible = helper.checkIsAllChildRowsVisible(0);
                res += isAllChildVisible.ToString();

                firstExpandButton.Click();
                Thread.Sleep(300);

                // check if after click all childs are hidden after clicking the collapse button
                bool isAllChildsHidden = helper.checkIsAllChildRowsHidden(0);
                res += isAllChildsHidden.ToString();

                Assert.AreEqual("TrueTrue", res);


            }
            finally
            {
                browser.Quit();
            }
        }

        #endregion


        #region Test_Expand_Mouse

        [TestMethod]
        [TestCategory("FlexGrid")]
        [Description("To test whether the group rows can be expanded using mouse")]
        public void TC_Expand_Mouse()
        {
            C.Execute_Test("TC_Expand_Mouse_Test");
        }

        public void TC_Expand_Mouse_Test(RemoteWebDriver browser)
        {
            browser.Manage().Window.Maximize();

            try
            {
                browser.Navigate().GoToUrl("https://silko11qa.outsystems.net/Test_DataGridSample_Stable/Grid.aspx");
                Thread.Sleep(300);

                IWebElement firstGroupCell = browser.FindElementByCssSelector(".wj-flexgrid .wj-cells .wj-row .wj-group");
                IWebElement firstExpandButton = firstGroupCell.FindElement(By.CssSelector(".wj-elem-collapse"));

                string res = "";

                GridJsHelper helper = new GridJsHelper(browser);

                // check if all childs are initially visible
                bool isAllChildVisible = helper.checkIsAllChildRowsVisible(0);
                res += isAllChildVisible.ToString();

                // collapse the group row
                firstExpandButton.Click();
                Thread.Sleep(300);

                // check if after click all childs are hidden
                bool isAllChildsHidden = helper.checkIsAllChildRowsHidden(0);
                res += isAllChildsHidden.ToString();

                // expand the group row
                firstGroupCell = browser.FindElementByCssSelector(".wj-flexgrid .wj-cells .wj-row .wj-group");
                firstExpandButton = firstGroupCell.FindElement(By.CssSelector(".wj-elem-collapse"));
                firstExpandButton.Click();
                Thread.Sleep(300);

                // check if all childs are visible
                isAllChildVisible = helper.checkIsAllChildRowsVisible(0);
                res += isAllChildVisible.ToString();

                Assert.AreEqual("TrueTrueTrue", res);


            }
            finally
            {
                browser.Quit();
            }
        }

        #endregion

        #region Test_Collapse_Keyboard

        [TestMethod]
        [TestCategory("FlexGrid")]
        [Description("To test whether the group rows can be collapsed using keyboard")]
        public void TC_Collapse_Keyboard()
        {
            C.Execute_Test("TC_Collapse_Keyboard_Test");
        }

        public void TC_Collapse_Keyboard_Test(RemoteWebDriver browser)
        {
            browser.Manage().Window.Maximize();

            try
            {
                browser.Navigate().GoToUrl("https://silko11qa.outsystems.net/Test_DataGridSample_Stable/Grid.aspx");
                Thread.Sleep(300);

                IWebElement firstGroupCell = browser.FindElementByCssSelector(".wj-flexgrid .wj-cells .wj-row .wj-group");
                IWebElement rowEl = firstGroupCell.FindElement(By.XPath("ancestor::*[contains(@class, 'wj-row')]"));

                string res = "";

                GridJsHelper helper = new GridJsHelper(browser);

                // check if all childs are initially visible
                bool isAllChildVisible = helper.checkIsAllChildRowsVisible(0);
                res += isAllChildVisible.ToString();

                // trigger back arrow key
                firstGroupCell.Click();
                Thread.Sleep(300);
                firstGroupCell.SendKeys(Keys.ArrowLeft);
                firstGroupCell.SendKeys(Keys.ArrowLeft);
                Thread.Sleep(300);

                // check if after click all childs are hidden after clicking the collapse button
                bool isAllChildsHidden = helper.checkIsAllChildRowsHidden(0);
                res += isAllChildsHidden.ToString();

                Assert.AreEqual("TrueTrue", res);


            }
            finally
            {
                browser.Quit();
            }
        }

        #endregion

        #region Test_Expand_Keyboard

        [TestMethod]
        [TestCategory("FlexGrid")]
        [Description("To test whether the group rows can be expanded using keyboard")]
        public void TC_Expand_Keyboard()
        {
            C.Execute_Test("TC_Expand_Keyboard_Test");
        }

        public void TC_Expand_Keyboard_Test(RemoteWebDriver browser)
        {
            browser.Manage().Window.Maximize();

            try
            {
                browser.Navigate().GoToUrl("https://silko11qa.outsystems.net/Test_DataGridSample_Stable/Grid.aspx");
                Thread.Sleep(300);

                IWebElement firstGroupCell = browser.FindElementByCssSelector(".wj-flexgrid .wj-cells .wj-row .wj-group");
                IWebElement rowEl = firstGroupCell.FindElement(By.XPath("ancestor::*[contains(@class, 'wj-row')]"));

                string res = "";

                GridJsHelper helper = new GridJsHelper(browser);

                // check if all childs are initially visible
                bool isAllChildVisible = helper.checkIsAllChildRowsVisible(0);
                res += isAllChildVisible.ToString();

                // trigger back arrow key
                firstGroupCell.Click();
                Thread.Sleep(300);
                firstGroupCell.SendKeys(Keys.ArrowLeft);
                firstGroupCell.SendKeys(Keys.ArrowLeft);
                Thread.Sleep(300);

                // check if after click all childs are hidden
                bool isAllChildsHidden = helper.checkIsAllChildRowsHidden(0);
                res += isAllChildsHidden.ToString();

                // expand the group row
                firstGroupCell = browser.FindElementByCssSelector(".wj-flexgrid .wj-cells .wj-row .wj-group");
                firstGroupCell.Click();
                Thread.Sleep(300);
                firstGroupCell.SendKeys(Keys.ArrowLeft);
                firstGroupCell.SendKeys(Keys.ArrowRight);
                Thread.Sleep(300);

                // check if all childs are visible after expand
                isAllChildVisible = helper.checkIsAllChildRowsVisible(0);
                res += isAllChildVisible.ToString();

                Assert.AreEqual("TrueTrueTrue", res);


            }
            finally
            {
                browser.Quit();
            }
        }

        #endregion

        #region Test_EditNumberFormat

        [TestMethod]
        [TestCategory("FlexGrid")]
        [Description("To test whether all decimal places are shown while editing in the grid when 'n' format is applied")]
        public void TC_EditFormatNumber()
        {
            C.Execute_Test("TC_EditFormatNumber_Test");
        }

        public void TC_EditFormatNumber_Test(RemoteWebDriver browser)
        {
            browser.Manage().Window.Maximize();

            try
            {
                browser.Navigate().GoToUrl("https://silko11qa.outsystems.net/Test_DataGridSample_Stable/Grid.aspx");

                Thread.Sleep(500);

                GridJsHelper helper = new GridJsHelper(browser);

                // get the first item in the collection view
                var firstItem = helper.GetDataItems().First();
                var columnBindings = helper.GetBindingColumns();

                string binding = "";

                // find a column containing number
                foreach (string key in columnBindings)
                {
                    object val = firstItem[key];
                    if (float.TryParse(val.ToString(), out _))
                    {
                        binding = key;
                        break;
                    }
                }

                // if not found
                // then throw exception
                if (binding.Equals(""))
                {
                    throw new NotFoundException("A number type column not found");
                }

                helper.SetColumnProperty(binding, "format", "n");

                // get the current column index
                int currentColumnIndex = int.Parse(helper.GetColumnProperty(binding, "index").ToString());

                // scroll to the column
                helper.ScrollIntoView(-1, currentColumnIndex);

                // get the DOM index of the column
                int colIdx = helper.GetColumnDOMIndex(binding);

                // get the index of the first data row
                int rowIdx = helper.FindFirstDataRowIndex();

                // get the cell element of current binding column
                var cell = helper.GetCellElement("wj-cells", rowIdx - 1, colIdx);

                // get the actual data using the collection view
                string currentDataValue = helper.GetItem(0, binding).ToString();

                // edit the cell
                cell.Click();
                cell.Click();

                Thread.Sleep(300);

                // get the editor value and parse the numbers from it
                var editorValue = cell.FindElement(By.CssSelector(".wj-grid-editor")).GetAttribute("value");
                editorValue = helper.ParseNumber(editorValue);

                // assert the result
                Assert.AreEqual(currentDataValue, editorValue);

            }
            finally
            {
                browser.Quit();
            }
        }

        #endregion

        #region Test_EditCurrencyFormat

        [TestMethod]
        [TestCategory("FlexGrid")]
        [Description("To test whether all decimal places are shown while editing in the grid when 'c' format is applied")]
        public void TC_EditFormatCurrency()
        {
            C.Execute_Test("TC_EditFormatCurrency_Test");
        }

        public void TC_EditFormatCurrency_Test(RemoteWebDriver browser)
        {
            browser.Manage().Window.Maximize();

            try
            {
                browser.Navigate().GoToUrl("https://silko11qa.outsystems.net/Test_DataGridSample_Stable/Grid.aspx");

                Thread.Sleep(500);

                GridJsHelper helper = new GridJsHelper(browser);

                // get the first item in the collection view
                var firstItem = helper.GetDataItems().First();
                var columnBindings = helper.GetBindingColumns();

                string binding = "";

                // find a column containing number
                foreach (string key in columnBindings)
                {
                    object val = firstItem[key];
                    if (float.TryParse(val.ToString(), out _))
                    {
                        binding = key;
                        break;
                    }
                }

                // if not found
                // then throw exception
                if (binding.Equals(""))
                {
                    throw new NotFoundException("A number type column not found");
                }

                helper.SetColumnProperty(binding, "format", "c3");

                // get the current column index
                int currentColumnIndex = int.Parse(helper.GetColumnProperty(binding, "index").ToString());

                // scroll to the column
                helper.ScrollIntoView(-1, currentColumnIndex);

                // get the DOM index of the column
                int colIdx = helper.GetColumnDOMIndex(binding);

                // get the index of the first data row
                int rowIdx = helper.FindFirstDataRowIndex();

                // get the cell element of current binding column
                var cell = helper.GetCellElement("wj-cells", rowIdx - 1, colIdx);

                // get the actual data using the collection view
                string currentDataValue = helper.GetItem(0, binding).ToString();

                // edit the cell
                cell.Click();
                cell.Click();

                Thread.Sleep(300);

                // get the editor value and parse the numbers from it
                var editorValue = cell.FindElement(By.CssSelector(".wj-grid-editor")).GetAttribute("value");
                editorValue = helper.ParseNumber(editorValue);

                // assert the result
                Assert.AreEqual(currentDataValue, editorValue);
            }
            finally
            {
                browser.Quit();
            }
        }

        #endregion

        #region Test_EditPercentageFormat

        [TestMethod]
        [TestCategory("FlexGrid")]
        [Description("To test whether all decimal places are shown while editing in the grid when 'p' format is applied")]
        public void TC_EditFormatPercentage()
        {
            C.Execute_Test("TC_EditFormatPercentage_Test");
        }

        public void TC_EditFormatPercentage_Test(RemoteWebDriver browser)
        {
            browser.Manage().Window.Maximize();

            try
            {
                browser.Navigate().GoToUrl("https://silko11qa.outsystems.net/Test_DataGridSample_Stable/Grid.aspx");

                Thread.Sleep(500);

                GridJsHelper helper = new GridJsHelper(browser);

                // get the first item in the collection view
                var firstItem = helper.GetDataItems().First();
                var columnBindings = helper.GetBindingColumns();

                string binding = "";

                // find a column containing number
                foreach (string key in columnBindings)
                {
                    object val = firstItem[key];
                    if (float.TryParse(val.ToString(), out _))
                    {
                        binding = key;
                        break;
                    }
                }

                // if not found
                // then throw exception
                if (binding.Equals(""))
                {
                    throw new NotFoundException("A number type column not found");
                }

                helper.SetColumnProperty(binding, "format", "p");

                // get the current column index
                int currentColumnIndex = int.Parse(helper.GetColumnProperty(binding, "index").ToString());

                // scroll to the column
                helper.ScrollIntoView(-1, currentColumnIndex);

                // get the DOM index of the column
                int colIdx = helper.GetColumnDOMIndex(binding);

                // get the index of the first data row
                int rowIdx = helper.FindFirstDataRowIndex();

                // get the cell element of current binding column
                var cell = helper.GetCellElement("wj-cells", rowIdx - 1, colIdx);

                // get the actual data using the collection view
                string currentDataValue = helper.GetItem(0, binding).ToString();

                // edit the cell
                cell.Click();
                cell.Click();

                Thread.Sleep(300);

                // get the editor value and parse the numbers from it
                var editorValue = cell.FindElement(By.CssSelector(".wj-grid-editor")).GetAttribute("value");
                editorValue = helper.ParseNumber(editorValue);

                // assert the result
                Assert.AreEqual(currentDataValue, editorValue);
            }
            finally
            {
                browser.Quit();
            }
        }

        #endregion

        #region Test_EditDecimalFormat

        [TestMethod]
        [TestCategory("FlexGrid")]
        [Description("To test whether all decimal places are shown while editing in the grid when 'p' format is applied")]
        public void TC_EditFormatDecimal()
        {
            C.Execute_Test("TC_EditFormatDecimal_Test");
        }

        public void TC_EditFormatDecimal_Test(RemoteWebDriver browser)
        {
            browser.Manage().Window.Maximize();

            try
            {
                browser.Navigate().GoToUrl("https://silko11qa.outsystems.net/Test_DataGridSample_Stable/Grid.aspx");

                Thread.Sleep(500);

                GridJsHelper helper = new GridJsHelper(browser);

                // get the first item in the collection view
                var firstItem = helper.GetDataItems().First();
                var columnBindings = helper.GetBindingColumns();

                string binding = "";

                // find a column containing number
                foreach (string key in columnBindings)
                {
                    object val = firstItem[key];
                    if (float.TryParse(val.ToString(), out _))
                    {
                        binding = key;
                        break;
                    }
                }

                // if not found
                // then throw exception
                if (binding.Equals(""))
                {
                    throw new NotFoundException("A number type column not found");
                }

                helper.SetColumnProperty(binding, "format", "d");

                // get the current column index
                int currentColumnIndex = int.Parse(helper.GetColumnProperty(binding, "index").ToString());

                // scroll to the column
                helper.ScrollIntoView(-1, currentColumnIndex);

                // get the DOM index of the column
                int colIdx = helper.GetColumnDOMIndex(binding);

                // get the index of the first data row
                int rowIdx = helper.FindFirstDataRowIndex();

                // get the cell element of current binding column
                var cell = helper.GetCellElement("wj-cells", rowIdx - 1, colIdx);

                // get the actual data using the collection view
                string currentDataValue = helper.GetItem(0, binding).ToString();
                currentDataValue = helper.GetFormattedNumber(currentDataValue, "d");

                // edit the cell
                cell.Click();
                cell.Click();

                Thread.Sleep(300);

                // get the editor value and parse the numbers from it
                var editorValue = cell.FindElement(By.CssSelector(".wj-grid-editor")).GetAttribute("value");
                editorValue = helper.ParseNumber(editorValue);

                // assert the result
                Assert.AreEqual(currentDataValue, editorValue);
            }
            finally
            {
                browser.Quit();
            }
        }

        #endregion

    }
}