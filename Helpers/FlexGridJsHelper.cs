﻿using OpenQA.Selenium;
using OpenQA.Selenium.Remote;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Web.Script.Serialization;

namespace C1Wijmo5.TestScripts.Helpers
{
    class GridJsHelper
    {

        public List<Dictionary<string, object>> GetDataItems()
        {
            ReadOnlyCollection<object> listItems = (ReadOnlyCollection<object>)browser.ExecuteScript("return window['grid'].collectionView.items");
            List<Dictionary<string, object>> items = new List<Dictionary<string, object>>();
            foreach (object item in listItems)
            {
                items.Add((Dictionary<string, object>)item);
            }
            return items;
        }

        /// <summary>
        /// Gets the particular property of a particular column
        /// </summary>
        /// <param name="binding">Binding of the column</param>
        /// <param name="property">Property of the column</param>
        /// <returns>The value of property of the column</returns>
        public object GetColumnProperty(string binding, string property)
        {
            return browser.ExecuteScript("return grid.columns.getColumn(arguments[0])[arguments[1]]", binding, property);
        }


        /// <summary>
        /// Gets the particular property of a particular column
        /// </summary>
        /// <param name="colIdx">Index of the column</param>
        /// <param name="property">Property of the column</param>
        /// <returns>The value of property of the column</returns>
        public object GetColumnProperty(int colIdx, string property)
        {
            return browser.ExecuteScript("return grid.columns[arguments[0]][arguments[1]]", colIdx, property);
        }

        public RemoteWebDriver browser { get; private set; }

        public GridJsHelper(RemoteWebDriver browser)
        {
            this.browser = browser;
            browser.ExecuteScript("window['grid'] = wijmo.Control.getControl('.wj-flexgrid')");
        }

        public string GetColumnHeader(int colIdx)
        {
            string header = browser.ExecuteScript("return window['grid'].columns[arguments[0]].header", colIdx).ToString();
            return header;
        }

        public string GetColumnHeader(string colBinding)
        {
            string header = browser.ExecuteScript("return window['grid'].columns.getColumn(arguments[0]).header", colBinding).ToString();
            return header;
        }



        /// <summary>
        /// Formats a date a particular format
        /// </summary>
        /// <param name="date">The date to format in string</param>
        /// <returns>The "d" formatted date</returns>
        public string GetFormattedDate(string date, string format = "d")
        {
            return browser.ExecuteScript("return wijmo.Globalize.formatDate(new Date(arguments[0]), arguments[1])", date, format).ToString();
        }

        public IWebElement GetCellElement(string selector, int row, int col)
        {
            return browser.FindElement(By.ClassName("wj-flexgrid")).FindElement(By.ClassName(selector)).FindElements(By.ClassName("wj-row"))[row].FindElements(By.ClassName("wj-cell"))[col];
        }

        /// <summary>
        /// To find out the single cell in Merged FlexGrid within FrozenColumns
        /// </summary>
        /// <returns>Returns CellInfo of row and Column</returns>
        public FlexGridCellInfo GetSingleCellInfo()
        {
            string script = "window['mergeCell']='';for(var i=0;i<grid.rows.length-1;i++){";
            script += "for(var j=0;j<grid.frozenColumns;j++){";
            script += "if(grid.getMergedRange(grid.cells,i,j,true).isSingleCell){ window['mergeCell'] = {row:i,col:j}; break;}";
            script += "}}";
            browser.ExecuteScript(script);
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            FlexGridCellInfo info = serializer.ConvertToType<FlexGridCellInfo>(browser.ExecuteScript("return mergeCell;"));
            return info;
        }

        /// <summary>
        /// Scroll the FlexGrid in View for specified Row and Column
        /// </summary>
        /// <param name="row">Row Index </param>
        /// <param name="col">Column Index</param>
        public void ScrollIntoView(int row, int col)
        {
            string script = "grid.scrollIntoView(" + row + "," + col + ");";
            browser.ExecuteScript(script);
        }

        /// <summary>
        /// Returns number of Columns in the FlexGrid
        /// </summary>
        /// <returns></returns>
        public int GetColumnCount()
        {
            return Convert.ToInt32(browser.ExecuteScript("return grid.columns.length"));
        }



        public object GetItem(int rowIdx, string binding)
        {
            var item = browser.ExecuteScript(string.Format("return grid.collectionView.items[{0}][\"{1}\"]", rowIdx, binding));
            return item;
        }


        public int FindFirstDataRowIndex()
        {
            var rows = browser.FindElementsByCssSelector(".wj-cells .wj-row");
            for (int row = 1; row < rows.Count; row++)
            {
                var cells = rows[row].FindElements(By.CssSelector(".wj-group"));
                if (cells.Count == 0)
                {
                    return row;
                }
            }
            return -1;
        }


        public long GetVisibleGroupRowCount()
        {
            string script = @"
                let count = 0; 
                grid.rows.forEach(function(r) {
                    if(r instanceof wijmo.grid.GroupRow && r.isVisible){
                        count++
                    }
                }); 
                return count;";

            return (long)browser.ExecuteScript(script);
        }

        public string GetFormattedNumber(string number)
        {
            return GetFormattedNumber(number, "n");
        }

        public string GetFormattedNumber(string number, string format)
        {
            return browser.ExecuteScript("return wijmo.Globalize.formatNumber(+arguments[0], arguments[1])", number, format).ToString();
        }

        public string ParseNumber(string number)
        {
            var parsedNumber = browser.ExecuteScript("return wijmo.Globalize.parseFloat(arguments[0])", number).ToString();
            return parsedNumber;
        }

        public void SetColumnProperty(string binding, string property, object value)
        {
            browser.ExecuteScript("return grid.columns.getColumn(arguments[0])[arguments[1]] = arguments[2]", binding, property, value);
        }
        public bool checkIsAllChildRowsVisible(int groupRowIndex)
        {
            string script = "" +
                "let allVisible = true;" +
                $"let range = grid.rows[{groupRowIndex}].getCellRange();" +
                "for(let i = range.topRow + 1; i <= range.bottomRow; i++ ) {" +
                    "let r = grid.rows[i];" +
                    "if(r instanceof wijmo.grid.GroupRow && r.isCollapsed){" +
                        "i = r.getCellRange().bottomRow;" +
                    "}" +
                    "if(!r.isVisible){" +
                        "allVisible = false;" +
                        "break;" +
                    "}" +
                "}" +
                "return allVisible";

            return (bool)browser.ExecuteScript(script);
        }

        public bool checkIsAllChildRowsHidden(int groupRowIndex)
        {
            string script = "" +
                "let allHidden = true;" +
                $"let range = grid.rows[{groupRowIndex}].getCellRange();" +
                "for(let i = range.topRow + 1; i <= range.bottomRow; i++ ) {" +
                    "let r = grid.rows[i];" +
                    "if(r.isVisible){" +
                        "allHidden = false;" +
                        "break;" +
                    "}" +
                "}" +
                "return allHidden";

            return (bool)browser.ExecuteScript(script);
        }

        public object GetFlexGridProperty(string property)
        {
            return browser.ExecuteScript(string.Format("return grid[\"{0}\"]", property));
        }

        /// <summary>
        /// Gets the Column Index in FlexGrid
        /// </summary>
        /// <param name="header"></param>
        /// <returns></returns>
        public int GetColumnDOMIndex(string binding)
        {

            // get viewrange
            // get visible columns
            // use visibleIndex to calculate the index
            var viewRange = (Dictionary<string, object>)GetFlexGridProperty("viewRange");

            var vIdx = int.Parse(GetColumnProperty(binding, "visibleIndex").ToString());

            var colIdx = vIdx - int.Parse(viewRange["col"].ToString());

            return colIdx;
        }
        /// <summary>
        /// Gets the list of bindings of each column
        /// </summary>
        /// <returns>A list containing all the bindings of the columns</returns>
        public List<string> GetBindingColumns()
        {
            List<string> bindings = new List<string>();
            int total = GetColumnCount();

            for (int col = 0; col < total; col++)
            {
                string b = GetColumnProperty(col, "binding").ToString();
                bindings.Add(b);
            }
            return bindings;
        }

        public bool CheckIfDataTypeCouldBeGrouped(RemoteWebDriver browser, string dataType)
        {
            // get grid object
            browser.ExecuteScript($"window['gridObj'] = GridOS.ComponentUtils.getGridObjectById()");

            // get column for data type
            var colBindKey = browser.ExecuteScript($"return Object.keys(gridObj.columns.rendered).find(colBind => gridObj.columns.rendered[colBind].type == '{dataType}')");

            // create group
            browser.ExecuteScript($"gridObj.grid.collectionView.groupDescriptions.clear()");
            browser.ExecuteScript($"gridObj.grid.collectionView.groupDescriptions.push(new wijmo.collections.PropertyGroupDescription('{colBindKey}'))");

            // get groups count
            long groupsCount = (long)browser.ExecuteScript($"return gridObj.grid.collectionView.groups.length");

            // get group rows count
            long gRowsCount = (long)browser.ExecuteScript($"return gridObj.grid.rows.filter(g => g instanceof wijmo.grid.GroupRow && g.dataItem.groupDescription.propertyName == '{colBindKey}').length");

            return groupsCount > 0 && groupsCount == gRowsCount;
        }
    }

}
