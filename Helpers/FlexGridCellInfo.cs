﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace C1Wijmo5.TestScripts.Helpers
{
    public class FlexGridCellInfo
    {
        public int row { get; set; }
        public int col { get; set; }
    }
}
