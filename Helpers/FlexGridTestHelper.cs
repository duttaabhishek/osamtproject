﻿using OpenQA.Selenium;
using OpenQA.Selenium.Remote;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

namespace C1Wijmo5.TestScripts.Helpers
{
    class FlexGridTestHelper
    {
        public List<string> GetCellsData(ReadOnlyCollection<IWebElement> rows, int index = 0)
        {
            List<string> cellsData = new List<string>();
            for (int curRow = 1; curRow < rows.Count; curRow++)
            {
                var cell = rows[curRow].FindElements(By.CssSelector(".wj-cell"))[index];
                cellsData.Add(cell.Text);
            }
            return cellsData;
        }


        public List<Dictionary<string, object>> GetDataItems(RemoteWebDriver browser)
        {
            ReadOnlyCollection<object> listItems = (ReadOnlyCollection<object>)browser.ExecuteScript("return window['grid'].collectionView.items");
            List<Dictionary<string, object>> items = new List<Dictionary<string, object>>();
            foreach (object item in listItems)
            {
                items.Add((Dictionary<string, object>)item);
            }
            return items;
        }

        /// <summary>
        /// Gets the particular property of a particular column
        /// </summary>
        /// <param name="colIdx">Index of the column</param>
        /// <param name="property">Property of the column</param>
        /// <returns>The value of property of the column</returns>
        public object GetColumnProperty(int colIdx, string property,RemoteWebDriver browser)
        {
            return browser.ExecuteScript("return grid.columns[arguments[0]][arguments[1]]", colIdx, property);
        }


        public class ItemsComparer : IEqualityComparer<Dictionary<string, object>>
        {
            public string Binding { get; set; }
            public bool Equals(Dictionary<string, object> first, Dictionary<string, object> second)
            {
                return first[Binding].ToString().Equals(second[Binding].ToString());
            }

            public int GetHashCode(Dictionary<string, object> obj)
            {
                return obj.GetHashCode();
            }
        }

        /// <summary>
        /// Returns number of Columns in the FlexGrid
        /// </summary>
        /// <returns></returns>
        public int GetColumnCount(RemoteWebDriver browser)
        {
            return Convert.ToInt32(browser.ExecuteScript("return grid.columns.length"));
        }

  

        /// <summary>
        /// Gets the list of bindings of each column
        /// </summary>
        /// <returns>A list containing all the bindings of the columns</returns>
        public List<string> GetBindingColumns(RemoteWebDriver browser)
        {
            List<string> bindings = new List<string>();
            int total = GetColumnCount(browser);

            for (int col = 0; col < total; col++)
            {
                string b = GetColumnProperty(col, "binding", browser).ToString();
                bindings.Add(b);
            }
            return bindings;
        }
    }
}
